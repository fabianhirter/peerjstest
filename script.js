const peer = new Peer();
let conn;

peer.on('open', function(id) {
    console.log('My peer ID is: ' + id);
});

function connect() {
    const peerId = document.getElementById("peer_id").value;
    conn = peer.connect(peerId);
}

function send() {
    const messageElement = document.getElementById("message");
    let message = messageElement.value;
    console.log("send:"+message);
    conn.send(message);
}

const connectButton = document.getElementById("connect");
connectButton.addEventListener("click", connect);

const sendButton = document.getElementById("send");
sendButton.addEventListener("click", send)

peer.on('connection', function(conn) {
    conn.on('data', function(data){
        // Will print 'hi!'
        console.log("receive:" + data);
    });
});